import dotenv from 'dotenv';
import { Client } from './../entities/client';
import { Event } from './../entities/event';

// If .env file is available use it
dotenv.config();

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

export default {
  port: parseInt(process.env.PORT, 10),

  api: {
    prefix: '/api',
  },

  logs: {
    level: process.env.LOG_LEVEL || 'debug',
  },

  typeorm: {
    name: 'default',
    type: 'postgres',
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT, 10),
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    synchronize: true,
    entities: [Client, Event],
    dropSchema: process.env.NODE_ENV === 'test',
  },
};
