import { createConnection } from 'typeorm';

export default async config => createConnection(config);
