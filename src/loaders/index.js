import expressLoader from './express';
import Logger from './logger';
import connection from './connection';
import config from '../config';

export default async ({ expressApp }) => {
  try {
    await connection(config.typeorm);
  } catch (e) {
    Logger.error(e);
    throw e;
  }
  Logger.info('DB connection established');

  await expressLoader({ app: expressApp });
  Logger.info('Express loaded');
};
