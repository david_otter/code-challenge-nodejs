import { Client } from '../models/client';
import operations from '../models/operations';
import { Client as ClientEntity } from '../entities/client';
import { Event } from '../entities/event';
// eslint-disable-next-line
import { getRepository, Repository } from 'typeorm';
import Logger from '../loaders/logger';

/**
 * The ClientService to perform business logic
 * and act as layer between routes and the database (Repository pattern)
 */
export class ClientService {
  /**
   * @type Repository
   */
  repository;
  auditLog;

  constructor() {
    this.repository = getRepository(ClientEntity);
    this.auditLog = getRepository(Event);
  }

  /**
   *
   * @type {String} id
   * @return {void}
   */
  async deleteClient(id) {
    Logger.debug(`deleting client ${id}`);

    this.auditLog.insert({ operation: operations.DELETE, data: { id } });

    return this.repository.delete({ id });
  }

  /**
   *
   * @param {String} id
   * @return {Client} The client
   */
  async getClientById(id) {
    Logger.debug(`fetching client ${id}`);

    const client = await this.repository.findOne({ id });

    return client ? new Client(client) : null;
  }

  /**
   *
   * @param {number} size The page size
   * @param {number} page The current page number
   * @param {Object} filters Additional filters to query
   * @return {Client[]} A paginated list of active clients
   */
  async getClients(size = 10, page = 1, filters) {
    Logger.debug(`fetching clients`, { size, page, filters });

    const [results, totalElements] = await this.repository.findAndCount({
      where: { active: true, ...filters },
      order: { lastName: 'DESC' },
      take: size,
      skip: size * (page - 1),
    });

    return {
      content: results.map(result => new Client(result)),
      page: {
        size,
        totalElements,
        page,
        totalPages: Math.ceil(totalElements / size),
      },
    };
  }

  /**
   *
   * @param {Client} client
   * @return {Client} The newly created client
   */
  async createClient(client) {
    Logger.debug('creating new client', { client });

    const clientEntity = new ClientEntity();
    Object.assign(clientEntity, client);

    const newClient = await this.repository.save(clientEntity);

    this.auditLog.insert({
      operation: operations.CREATE,
      data: { client, id: newClient.id },
    });

    return new Client(newClient);
  }

  /**
   *
   * @param {String} id
   * @param {Object} clientUpdate
   * @return {Client} The updated client
   */
  async updateClient(id, clientUpdate) {
    Logger.debug(`updating client ${id}`, { clientUpdate });

    await this.repository.update({ id }, clientUpdate);

    this.auditLog.insert({
      operation: operations.UPDATE,
      data: { id, clientUpdate },
    });

    const updatedClient = await this.repository.findOne({ id });

    return new Client(updatedClient);
  }
}
