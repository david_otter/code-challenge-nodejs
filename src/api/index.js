import { Router } from 'express';
import client from './routes/client';

export default () => {
  const app = Router();

  // Register all routes which should get exposed
  client(app);

  return app;
};
