import { Router } from 'express';
import { celebrate, Joi } from 'celebrate';
import { Client as ClientModel } from '../../models/client';
import { ClientService } from '../../services/client';
import Logger from '../../loaders/logger';

export default app => {
  const route = Router();
  app.use('/clients', route);

  const clientService = new ClientService();

  route.get(
    '/:id',
    celebrate({
      params: Joi.object({
        id: Joi.string().required(),
      }),
    }),
    async (req, res) => {
      const id = req.params.id;

      try {
        const client = await clientService.getClientById(id);

        if (!client) {
          return res.status(404).send();
        }

        return res.status(200).json(client);
      } catch (e) {
        Logger.error(`fetching the client ${id} failed`, e);
        return res.status(500).send();
      }
    },
  );

  route.get(
    '/',
    celebrate({
      query: Joi.object({
        page: Joi.number()
          .default(1)
          .min(1),
        size: Joi.number()
          .default(10)
          .min(1)
          .max(1000),
        lastName: Joi.string(),
        postalCode: Joi.string(),
        city: Joi.string(),
        country: Joi.string(),
      }),
    }),
    async (req, res) => {
      const { page, size, ...filters } = req.query;

      try {
        const client = await clientService.getClients(size, page, filters);

        return res.status(200).json(client);
      } catch (e) {
        Logger.error(`fetching client failed`, e);
        return res.status(500).send();
      }
    },
  );

  route.post(
    '/',
    celebrate({
      body: Joi.object({
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        telephoneNumber: Joi.string().required(),
        emailAddress: Joi.string().required(),
        street: Joi.string().required(),
        postalCode: Joi.string().required(),
        city: Joi.string().required(),
        country: Joi.string().required(),
      }),
    }),
    async (req, res) => {
      const client = new ClientModel(req.body);

      try {
        const newClient = await clientService.createClient(client);
        return res.status(201).json(newClient);
      } catch (e) {
        Logger.error('creating the client failed', e);
        return res.status(500).send();
      }
    },
  );

  route.patch(
    '/:id',
    celebrate({
      params: Joi.object({
        id: Joi.string().required(),
      }),
      body: Joi.object({
        firstName: Joi.string(),
        lastName: Joi.string(),
        telephoneNumber: Joi.string(),
        emailAddress: Joi.string(),
        street: Joi.string(),
        postalCode: Joi.string(),
        city: Joi.string(),
        country: Joi.string(),
        active: Joi.boolean(),
      }),
    }),
    async (req, res) => {
      const clientUpdate = req.body;
      if (Object.keys(clientUpdate).length === 0) {
        return res.status(400).send();
      }
      const id = req.params.id;

      try {
        const updatedClient = await clientService.updateClient(
          id,
          clientUpdate,
        );

        if (!updatedClient) {
          return res.status(404).send();
        }

        return res.status(200).json(updatedClient);
      } catch (e) {
        Logger.error(`updating the client ${id} failed`, e);
        return res.status(500).send();
      }
    },
  );

  route.delete(
    '/:id',
    celebrate({
      params: Joi.object({
        id: Joi.string().required(),
      }),
    }),
    async (req, res) => {
      const id = req.params.id;

      try {
        await clientService.deleteClient(id);
        return res.status(204).send();
      } catch (e) {
        Logger.error(`deleting the client ${id} failed`, e);
        return res.status(500).send();
      }
    },
  );
};
