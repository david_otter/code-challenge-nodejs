export class Client {
  constructor({
    id,
    firstName,
    lastName,
    telephoneNumber,
    emailAddress,
    street,
    postalCode,
    city,
    country,
  }) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.telephoneNumber = telephoneNumber;
    this.emailAddress = emailAddress;
    this.street = street;
    this.postalCode = postalCode;
    this.city = city;
    this.country = country;
  }

  id;
  firstName;
  lastName;
  telephoneNumber;
  emailAddress;
  street;
  postalCode;
  city;
  country;
}
