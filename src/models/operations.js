export default {
  DELETE: 'delete',
  CREATE: 'create',
  UPDATE: 'update',
};
