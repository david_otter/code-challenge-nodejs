import express from 'express';
import config from './config';

async function startServer() {
  const app = express();

  await require('./loaders').default({ expressApp: app });

  app.listen(config.port, err => {
    if (err) {
      console.error(err);
      process.exit(1);
      return;
    }
    console.info(`
      ################################################
      🛡️  Server listening on port: ${config.port} 🛡️ 
      ################################################
    `);
  });
}

startServer();

process
  .on('unhandledRejection', (reason, promise) => {
    console.error('Unhandled rejection', reason, promise);
  })
  .on('uncaughtException', err => {
    console.error('Uncaught Exception', err);
    process.exit(1);
  });
