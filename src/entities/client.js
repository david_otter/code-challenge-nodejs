const { Entity, Column, PrimaryGeneratedColumn } = require('typeorm');

@Entity()
class Client {
  @PrimaryGeneratedColumn('uuid')
  id;

  @Column('varchar')
  firstName;

  @Column('varchar')
  lastName;

  @Column('varchar')
  telephoneNumber;

  @Column('varchar')
  emailAddress;

  @Column('varchar')
  street;

  @Column('varchar')
  postalCode;

  @Column('varchar')
  city;

  @Column('varchar')
  country;

  @Column('boolean', {
    default: true,
  })
  active;
}

export { Client };
