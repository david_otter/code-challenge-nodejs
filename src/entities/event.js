const { Entity, Column, PrimaryGeneratedColumn } = require('typeorm');

@Entity()
class Event {
  @PrimaryGeneratedColumn('uuid')
  id;

  @Column('varchar')
  operation;

  @Column('json')
  data;
}

export { Event };
