import request from 'supertest';
import express from 'express';

const createClientFixture = () => ({
  firstName: 'Hans',
  lastName: 'Werner',
  telephoneNumber: '+43 664 516',
  emailAddress: 'me@pm.me',
  street: 'Soccerstreet',
  postalCode: '80799',
  city: 'Munich',
  country: 'DE',
});

describe('Post Endpoints', () => {
  let app;

  beforeAll(async () => {
    app = express();
    await require('../src/loaders').default({ expressApp: app });
  });

  it('should create a new client', async () => {
    const client = createClientFixture();

    const res = await request(app)
      .post('/api/clients')
      .send(client)
      .expect(201);

    expect(res.body).toEqual({ id: res.body.id, ...client });
  });

  it('should get a client', async () => {
    const client = createClientFixture();

    const postClient = await request(app)
      .post('/api/clients')
      .send(client)
      .expect(201);

    const getClient = await request(app)
      .get(`/api/clients/${postClient.body.id}`)
      .expect(200);

    expect(getClient.body).toEqual({ id: postClient.body.id, ...client });
  });

  it('should delete a client', async () => {
    const client = createClientFixture();

    const postClient = await request(app)
      .post('/api/clients')
      .send(client)
      .expect(201);

    await request(app)
      .delete(`/api/clients/${postClient.body.id}`)
      .expect(204);
  });

  it('should patch a client', async () => {
    const client = createClientFixture();

    const postClient = await request(app)
      .post('/api/clients')
      .send(client)
      .expect(201);

    const updatedClient = await request(app)
      .patch(`/api/clients/${postClient.body.id}`)
      .send({ active: false, lastName: 'Mueller' })
      .expect(200);

    expect(updatedClient.body.lastName).toEqual('Mueller');
  });

  it('should find all clients (with pagination)', async () => {
    const client1 = createClientFixture();
    client1.country = 'UK';

    const client2 = createClientFixture();
    client2.country = 'UK';
    client2.lastName = 'Johnson';

    await request(app)
      .post('/api/clients')
      .send(client1)
      .expect(201);

    await request(app)
      .post('/api/clients')
      .send(client2)
      .expect(201);

    const clientsUK = await request(app)
      .get(`/api/clients?country=UK`)
      .expect(200);

    expect(clientsUK.body.content.length).toBe(2);
    expect(clientsUK.body.page.totalElements).toBe(2);

    const clientsJohnson = await request(app)
      .get(`/api/clients?lastName=Johnson&page=1&size=1`)
      .expect(200);

    expect(clientsJohnson.body.content.length).toBe(1);
    expect(clientsJohnson.body.page.totalElements).toBe(1);
    expect(clientsJohnson.body.page.size).toBe(1);
    expect(clientsJohnson.body.page.totalPages).toBe(1);
  });
});
