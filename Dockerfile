FROM node:12 AS base

WORKDIR /app
COPY package*.json ./

FROM base AS build

RUN npm ci
COPY . /app/
RUN npm run build

FROM build AS test

RUN npm audit --production
# Add additional scripts here like
# RUN npm run lint
# RUN npm run test

FROM base AS dependencies

RUN npm ci --only=production

FROM node:12-alpine AS release

USER node
RUN mkdir -p /home/node/app
WORKDIR /home/node/app

COPY --from=dependencies --chown=node:node /app/node_modules ./node_modules
COPY --from=dependencies --chown=node:node /app/package*.json ./
COPY --from=build --chown=node:node /app/dist ./
# This is evil - do not do such things if you wanna deploy your application
# Better set your process.env variables
COPY --chown=node:node .env ./

EXPOSE 3000

CMD ["node", "main.js"]
