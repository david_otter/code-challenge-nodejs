# code-challenge

## Getting Started

### Prerequisites

Before starting, make sure you have at least those components on your wokstation:

 - [NodeJS](https://nodejs.org/en/download) >= 12
 - [Docker](https://www.docker.com/get-started) and [docker-compose](https://docs.docker.com/compose/) 
 
### Setup

Start the service by running
````shell
docker-compose up --build
````

### API endpoints

```
GET /api/clients

GET /api/clients/:id

POST /api/clients

PATCH /api/clients/:id

DELETE /api/clients/:id
```

Examples:

```
POST /api/clients
{
  "firstName": "Hans",
  "lastName": "Werner",
  "telephoneNumber": "+43 664 516",
  "emailAddress": "me@pm.me",
  "street": "Soccerstreet",
  "postalCode": "80799",
  "city": "Munich",
  "country": "DE"
}
```

```
PATCH /api/clients/04e59899-1db0-4d0b-925f-907a95236f2d
{
  "firstName": "Hans",
  "lastName": "Werner",
  "telephoneNumber": "+43 664 516",
  "emailAddress": "me@pm.me",
  "street": "Soccerstreet",
  "postalCode": "80799",
  "city": "Munich",
  "country": "DE"
  "active": false
}
```

```
GET /api/clients?page=1&size=10&country=DE
```

### Development

First install all dependencies:
````shell
npm install
````
Then edit the `.env` file with your configuration.

To provide a local postgreSQL instance run:
````shell
docker-compose up postgres
````

To start the service run:
````shell
npm run start
````

For e2e tests first start the database and then run:
````shell
npm run test:e2e
````

### Disclaimer

The setup and structure of the project is inspired by [bulletproof-nodejs](https://github.com/santiq/bulletproof-nodejs)

Do **not** use this project as a template before reading the following:

- **Never** commit the `.env` file as part of your repository. Better provide a `.env.example` file
- For new projects I can highly recommend [TypeScript](https://www.typescriptlang.org/), which is a superset of JavaScript and also interoperable with `.js`files
- While [express](https://expressjs.com) is a very stable and mature framework there has not been much progress in the last years.
You might want to look into [fastify](https://www.fastify.io/) or more opinionated frameworks like [NestJs](https://nestjs.com/) or [AdonisJs](https://adonisjs.com/)

